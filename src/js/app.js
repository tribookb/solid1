App = {
    web3Provider: null,
    contracts: {},

    init: function () {
        return App.initWeb3();
    },

    initWeb3: function () {
        // Initialize web3 and set the provider to the testRPC.
        if (typeof web3 !== 'undefined') {
            App.web3Provider = web3.currentProvider;
            web3 = new Web3(web3.currentProvider);
        } else {
            // set the provider you want from Web3.providers
            App.web3Provider = new Web3.providers.HttpProvider('http://127.0.0.1:9545');
            web3 = new Web3(App.web3Provider);
        }

        return App.initContract();
    },

    initContract: function () {
        $.getJSON('ItrMintableToken.json', function (data) {
            // Get the necessary contract artifact file and instantiate it with truffle-contract.
            var TokenArtifact = data;
            App.contracts.ItrToken = TruffleContract(TokenArtifact);

            // Set the provider for our contract.
            App.contracts.ItrToken.setProvider(App.web3Provider);

            // Use our contract to retieve and mark the adopted pets.
            return App.getBalances();
        });

        return App.bindEvents();
    },

    bindEvents: function () {
        $(document).on('click', '#redeemButton', App.handleRedeem);
        $(document).on('click', '#totalButton', App.handleTotal);
    },

    handleRedeem: function (event) {
        event.preventDefault();

        var itrTokenInstance;

        web3.eth.getAccounts(function (error, accounts) {
            if (error) {
                console.log(error);
            }

            var account = accounts[0];
            console.log('Try to redeem IMT to ' + account);

            App.contracts.ItrToken.deployed().then(function (instance) {
                itrTokenInstance = instance;

                return itrTokenInstance.requestMint(account);
            }).then(function (result) {
                alert('Redeem Successful!');
                return App.getBalances();
            }).catch(function (err) {
                console.log(err.message);
            });
        });
    },

    handleTotal: function (event) {
        event.preventDefault();

        var itrTokenInstance;

        web3.eth.getAccounts(function (error, accounts) {
            if (error) {
                console.log(error);
            }

            var account = accounts[0];

            App.contracts.ItrToken.deployed().then(function (instance) {
                itrTokenInstance = instance;

                return itrTokenInstance.totalSupply();
            }).then(function (result) {
                alert('totalSupply() Successful! ' + result.c[0] + 'IMT');
            }).catch(function (err) {
                console.log(err.message);
            });
        });
    },

    getBalances: function () {
        console.log('Getting balances...');

        var tutorialTokenInstance;

        web3.eth.getAccounts(function (error, accounts) {
            if (error) {
                console.log(error);
            }

            var account = accounts[0];

            App.contracts.ItrToken.deployed().then(function (instance) {
                tutorialTokenInstance = instance;

                return tutorialTokenInstance.balanceOf(account);
            }).then(function (result) {
                balance = result.c[0];

                $('#Balance').text(balance);
            }).catch(function (err) {
                console.log(err.message);
            });
        });
    }

};

$(function () {
    $(window).load(function () {
        App.init();
    });
});
