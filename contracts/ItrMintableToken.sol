pragma solidity ^0.4.0;

import './ERC223/InternallyMintableToken.sol';

contract ItrMintableToken is InternallyMintableToken {
    string public name = "Itransition Mintable Token";
    string public symbol = "IMT";
    uint32 public decimals = 18;

    uint256 public constant maxCap = 1000;
    uint256 public constant mintStep = 10;

    uint256 public constant mintInterval = 10;

    uint256 public lastMint;

    modifier mintTimed() {
        require(now.sub(lastMint) >= mintInterval);
        _;
    }

    function ItrMintableToken() public {
        lastMint = now;
    }

    function requestMint(address _to) canMint public returns (bool) {
        mint(_to, mintStep);
        lastMint = now;
        if (totalSupply == maxCap) {
            finishMinting();
        }

        return true;
    }
}
